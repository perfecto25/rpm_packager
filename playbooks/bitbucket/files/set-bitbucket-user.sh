if [ -n "${BITBUCKET_USER}" ]; then
    return
fi

# START INSTALLER MAGIC ! DO NOT EDIT !
BITBUCKET_USER="atlbitbucket" # user created by installer
# END INSTALLER MAGIC ! DO NOT EDIT !

export BITBUCKET_USER
