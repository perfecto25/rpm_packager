
This repo contains packaging scripts to package various Atlassian apps as RPMs


To package an RPM, the packaging host OS needs FPM gem installed. To run the packager, 

1. run ```source secure``` to export secure vars (artif key)
1. edit pkg_bitbucket.sh - update the Installer $version
1. edit additionals/ open scripts and update $version
1. edit response.varfile update $version
1. pull down the install.bin file from Atlassian (or it will attempt to pull down for you)
1.  ```chmod +x pkg_bitbucket.sh```
1. ```cd bitbucket && ./pkg_bitbucket.sh```

Each application packager installs the app locally in $app_name/stage directory, creates an RPM and uploads the RPM to artifactory.