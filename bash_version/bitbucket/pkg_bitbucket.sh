#!/bin/bash
'
# ATLASSIAN BITBUCKET RPM PACKAGER
# 1. update Bitbucket $version
# 2. update DB name dependency
# 3. export ARTIF_KEY (source secure)
# 4. run pkg_bitbucket.sh

NOTE: Startup script is SYSTEMD, not INIT
NOTE: Bitbucket is very toughtly coupled with Postgres  -- make sure to update "after-install" script if making Postgres changes`
'

app='bitbucket'
version=5.5.0
RHEL_version='rhel7'
#db_name="postgresql96-9.6.5-1PGDG.${RHEL_version}"
db_name="postgresql96-server"
bin="atlassian-${app}-${version}-x64.bin"
#bitbucket_tar="atlassian-bitbucket-software-${version}.tar.gz"
atlassian_url="https://www.atlassian.com/software/stash/downloads/binary/${bin}"
install_dir="$(pwd)/stage/opt/atlassian/${app}"

source $(pwd)/../common
source $(pwd)/../secure

# check settings 
check_common_settings

echo "....cleaning previous build.."
cleanup

# get installer bin from Atlassian
[ -f installers/$bin ] || wget --no-check-certificate -P installers/ $atlassian_url

# check if varfile & startup exists
if [ ! -f installers/response.varfile ] || [ ! -f additionals/$app ]
then
	echo "startup script or silent installer varfile not present, exiting.."
	exit 1
fi

# update Version
prev_version=$(grep 'app.defaultInstallDir' installers/response.varfile | awk -F'=' {'print $2'} | awk -F'/' {'print $(NF)'})
sed -i -e "s/${prev_version}/${version}/g" installers/response.varfile

#prev_version=$(grep ExecStart additionals/bitbucket.service | awk -v FS="(bitbucket/|/bin)" '{print $2}')
#sed -i -e "s/${prev_version}/${version}/g" additionals/bitbucket.service

#prev_version=$(grep '^VERSION=' additionals/set-jre-home.sh | awk -F'=' {'print $2'})
#sed -i -e "s/${prev_version}/${version}/g" additionals/set-jre-home.sh

# update install path in varfile
prev_path=$(grep 'app.defaultInstallDir' installers/response.varfile | awk -F'=' {'print $2'})
sed -i "s|${prev_path}|${install_dir}/${version}|g" installers/response.varfile


echo "....installing $app via bin installer (using bin since tar installer lacks the JRE folder"
chmod +x installers/$bin
./installers/$bin -q -varfile response.varfile

# ADDITIONAL CUSTOMIZATIONS
echo "....placing Additional files into RPM"
cp -R additionals/jre $install_dir/$version
cp additionals/set-bitbucket-user.sh $install_dir/$version/bin/
cp additionals/set-jre-home.sh $install_dir/$version/bin/
rm -f $install_dir/$version/bin/*.exe
rm -f $install_dir/$version/bin/*.bat
# SystemD startup script
cp additionals/${app}.service /usr/lib/systemd/system/

# postgres conf
cp additionals/pg_hba.conf $install_dir/

echo "remove Version, place everything into /opt/atlassian/bitbucket"
mv $install_dir/$version/* $install_dir/
rm -rf $install_dir/$version


echo "....CREATING RPM from stage dir"
/usr/local/bin/fpm -s dir -t rpm \
--after-install additionals/after-install \
-C stage/ \
--name $app \
--version ${version} \
--iteration "1" \
--workdir $(pwd) \
--architecture x86_64 \
--maintainer "Atlassian" \
--description "Atlassian $app" \
--url "www.atlassian.com"  \
--package $(pwd)/RPM/${app}-${version}-1.x86_64.rpm \
-d $db_name opt/ /usr/lib/systemd/system/$app.service

# upload RPM to artifactory
echo "....uploading to Artifactory"
upload_to_artifactory

cleanup

echo "....done packaging RPM..."