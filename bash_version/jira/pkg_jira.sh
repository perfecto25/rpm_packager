#!/bin/bash
# ATLASSIAN JIRA RPM PACKAGER
# 1. update Jira $version
# 2. update DB name dependency and after-install script located in "additionals" dir
# 3. export ARTIF_KEY (source secure)
# 4. run pkg_jira.sh

# NOTE: Startup script is SYSTEMD, not INIT

app='jira'
version=7.5.0
db_name="mysql-community-server"
bin="atlassian-${app}*-${version}-x64.bin"
atlassian_url="https://downloads.atlassian.com/software/jira/downloads/${bin}"
install_dir="$(pwd)/stage/opt/atlassian/${app}"

source $(pwd)/../common
source $(pwd)/../secure

# check settings 
check_common_settings

echo "....cleaning previous build.."
cleanup

# get installer bin from Atlassian
[ -f installers/$bin ] || wget --no-check-certificate -P installers/ $atlassian_url

# check if varfile & startup exists
if [ ! -f installers/response.varfile ] || [ ! -f additionals/$app ]
then
	echo "startup script or silent installer varfile not present, exiting.."
	exit 1
fi

# update install path in varfile
prev_path=$(grep 'sys.installationDir' installers/response.varfile | awk -F'=' {'print $2'})
sed -i "s|${prev_path}|${install_dir}|g" installers/response.varfile

echo "....installing $app via bin installer"
chmod +x installers/$bin
./installers/$bin -q -varfile response.varfile

# ADDITIONAL CUSTOMIZATIONS
echo "....placing Additional files into RPM"
rm -f $install_dir/bin/*.exe
rm -f $install_dir/bin/*.bat
cp $(pwd)/../shared_files/mysql-connector* $install_dir/lib/
rm -f $install_dir/bin/setenv.sh
cp additionals/setenv.sh $install_dir/bin/

# SystemD startup script
cp additionals/${app}.service /usr/lib/systemd/system/

echo "....CREATING RPM from stage dir"
/usr/local/bin/fpm -s dir -t rpm \
--after-install additionals/after-install \
-C stage/ \
--name $app \
--version ${version} \
--iteration "1" \
--workdir $(pwd) \
--architecture x86_64 \
--maintainer "Atlassian" \
--description "Atlassian $app" \
--url "www.atlassian.com"  \
--package $(pwd)/RPM/${app}-${version}-1.x86_64.rpm \
-d $db_name opt/ /usr/lib/systemd/system/$app.service

# upload RPM to artifactory
echo "....uploading to Artifactory"
upload_to_artifactory

cleanup

echo "....done packaging RPM..."



