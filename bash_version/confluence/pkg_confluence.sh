#!/bin/bash
# ATLASSIAN Confluenec RPM PACKAGER
# 1. update Confluence $version
# 2. update DB name dependency and after-install script located in "additionals" dir
# 3. export ARTIF_KEY (source secure)
# 4. run pkg_confluence.sh

app='confluence'
version=6.4.1
db_name="mysql-community-server"
bin="atlassian-confluence-${version}-x64.bin"
atlassian_url="https://downloads.atlassian.com/software/confluence/downloads/${bin}"
install_dir="$(pwd)/stage/opt/atlassian/${app}"

source $(pwd)/../common

# check settings 
check_common_settings

echo "....cleaning previous build.."
cleanup

# get installer bin from Atlassian
[ -f installers/$bin ] || wget --no-check-certificate -P installers/ $atlassian_url

[ -d $install_dir ] || mkdir -p $install_dir

# check if varfile & startup exists
if [ ! -f installers/response.varfile ] || [ ! -f additionals/$app ]
then
	echo "startup script or silent installer varfile not present, exiting.."
	exit 1
fi


# update install path in varfile
prev_path=$(grep 'sys.installationDir' installers/response.varfile | awk -F'=' {'print $2'})
sed -i "s|${prev_path}|${install_dir}|g" installers/response.varfile

# fix startup script if confluence user is confluence1
# caused by: https://confluence.atlassian.com/confluencekb/how-to-set-the-user-confluence-runs-as-in-linux-433390559.html
userdel confluence

echo "....installing $app via bin installer"
chmod +x installers/$bin
./installers/$bin -q -varfile response.varfile

# ADDITIONAL CUSTOMIZATIONS
echo "....placing Additional files into RPM"
rm -f $install_dir/bin/*.exe
rm -f $install_dir/bin/*.bat
cp $(pwd)/../shared_files/mysql-connector* $install_dir/confluence/WEB-INF/lib/


echo "....CREATING RPM from stage dir"
/usr/local/bin/fpm -s dir -t rpm \
--rpm-init additionals/$app \
--after-install additionals/after-install \
-C stage/ \
--name $app \
--version ${version} \
--iteration "1" \
--workdir $(pwd) \
--architecture x86_64 \
--maintainer "Atlassian" \
--description "Atlassian $app" \
--url "www.atlassian.com"  \
--package $(pwd)/RPM/${app}-${version}-1.x86_64.rpm \
-d $db_name

# upload RPM to artifactory
echo "....uploading to Artifactory"
upload_to_artifactory

cleanup

echo "....done packaging RPM..."



# echo "CREATING confluence SOFTWARE RPM"
# /usr/local/bin/fpm -s dir -t rpm  --workdir $(pwd) --verbose --description "Atlassian confluence" \
# --architecture x86_64 --maintainer "Atlassian" --url "www.atlassian.com" --name atlassian-confluence --rpm-init confluence  \
# --version ${confluence_version} /opt/atlassian/confluence
# check4error "fpm make new RPM"

# rpm_name=$(ls *${confluence_version}*.rpm)


# [ -z $ARTIF_KEY ] && echo "Artifactory key env var ARTIF_KEY is not set, cannot upload RPM to Artifactory.. exiting" && exit 1;

# #echo "----- get md5 checksum"
# md5=$(md5sum $rpm_name | awk -F" " {'print $1'})

# #echo "----- get sha1 checksum"
# sha1=$(sha1sum $rpm_name | awk -F" " {'print $1'})

# #echo "----- upload RPM to artifactory using API key"
  
# curl -H "X-JFrog-Art-Api:${ARTIF_KEY}" -H "X-Checksum-Md5:${md5}" -H "X-Checksum-Sha1:${sha1}" -X PUT "http://${artif_server}:${artif_port}/artifactory/${artif_repo_path}/${rpm_name}" -T $rpm_name


# Cleanup
# uninstall bin
#/opt/atlassian/confluence/uninstall -q


