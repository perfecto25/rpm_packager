#!/bin/bash
# ATLASSIAN CROWD RPM PACKAGER
# 1. update Crowd $version
# 2. update DB name dependency and after-install script located in "additionals" dir
# 3. export ARTIF_KEY (source secure)
# 4. run pkg_crowd.sh

app='crowd'
version=3.0.1
RHEL_version='rhel6'
db_name="mysql-community-server"
bin="atlassian-${app}-${version}.tar.gz"
atlassian_url="https://downloads.atlassian.com/software/crowd/downloads/binary/${bin}"
install_dir="$(pwd)/stage/opt/atlassian/${app}"

source $(pwd)/../common
source $(pwd)/../secure

# check settings 
check_common_settings

echo "....cleaning previous build.."
cleanup

# get installer bin from Atlassian
[ -f installers/$bin ] || wget --no-check-certificate -P installers/ $atlassian_url

[ -d $install_dir ] || mkdir -p $install_dir

# untar crowd, change install dir
tar -xvzf installers/$bin -C $install_dir
mv $install_dir/atlassian-crowd*/* $install_dir
rm -rf $install_dir/atlassian-crowd-$version

rm -f $install_dir/crowd-webapp/WEB-INF/classes/crowd-init.properties
cp additionals/crowd-init.properties $install_dir/crowd-webapp/WEB-INF/classes/

rm -f $install_dir/apache-tomcat/bin/*.exe
rm -f $install_dir/apache-tomcat/bin/*.bat

# SystemD startup script
cp additionals/${app}.service /usr/lib/systemd/system/

# MySQL connector
cp $(pwd)/../shared_files/mysql-connector* $install_dir/apache-tomcat/lib/

echo "....CREATING RPM from stage dir"
/usr/local/bin/fpm -s dir -t rpm \
--after-install additionals/after-install \
-C stage/ \
--name $app \
--version ${version} \
--iteration "1" \
--workdir $(pwd) \
--architecture x86_64 \
--maintainer "Atlassian" \
--description "Atlassian $app" \
--url "www.atlassian.com"  \
--package $(pwd)/RPM/${app}-${version}-1.x86_64.rpm \
-d $db_name opt/ /usr/lib/systemd/system/$app.service

# upload RPM to artifactory
upload_to_artifactory

cleanup

echo "....done packaging RPM..."


