# Custom RPM packager (Ansible)
1. update 'hosts' file with your test target
2. in same dir as this Readme file, create secure.yaml file, add the following values,
```    
    artif_server: artifactory.corp.local
    artif_port: 8081
    artif_key: key
    artif_repo_path: altassian-rpms
```

3. update the atlassian product version in playbooks/pkg_app.yaml
4. run playbook to create RPM package

    ``` ansible-playbook -i hosts playbooks/pkg_fisheye.yaml -v ```
5. RPM will be uploaded to Artifactory based on path defined in 'secure.yaml'